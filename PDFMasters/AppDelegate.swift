//
//  AppDelegate.swift
//  PDFMasters
//
//  Created by Rashedul Hoque on 19/6/20.
//  Copyright © 2020 Rashedul Hoque. All rights reserved.
//  com.rashed.-maptrack.PDFMasters

//pdfreadermaster
//com.rashed.pdfreaderpro

import UIKit
import GoogleMobileAds

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,GADBannerViewDelegate,GADRewardedAdDelegate {
   
    

    var window: UIWindow?
    
 
    let bannerAddUitLiveID = ""// "ca-app-pub-3940256099942544/2934735716"  //  Test ID
   //   let rewardAddUitLiveID = "" // Test ID
  //  let bannerAddUitLiveID = "ca-app-pub-6906252190682832/9037764189"          //  Live ID
  //  let rewardAddUitLiveID = ""          // Live ID
    var adMobBannerView: GADBannerView!
    var adMobBannerOnNavigationBar: GADBannerView!
    

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        Helper.createDirectory()
        GADMobileAds.sharedInstance().start(completionHandler: nil)
        GADMobileAds.sharedInstance().requestConfiguration.testDeviceIdentifiers = ["phone 11 pro"];
              initializeGoogleAdmob()
           //   initializeGoogleAdmobOnNavigationBar()
        return true
    }

    
    func initializeGoogleAdmob() {
            
           adMobBannerView = GADBannerView(adSize: kGADAdSizeBanner)
           adMobBannerView.adUnitID = bannerAddUitLiveID
          
           adMobBannerView.delegate = self as GADBannerViewDelegate
           adMobBannerView.load(GADRequest())
           //adMobBannerView.isHidden = true
         
       
    }
    
    func initializeGoogleAdmobOnNavigationBar() {
            
             adMobBannerOnNavigationBar = GADBannerView(adSize: kGADAdSizeBanner)
             adMobBannerOnNavigationBar.adUnitID = bannerAddUitLiveID
             adMobBannerOnNavigationBar.delegate = self as GADBannerViewDelegate
             adMobBannerOnNavigationBar.load(GADRequest())
             
      }
    
    //
    // MARK GOOGLE ADMOB BANNER DELEGATE
    //
    
    /// Tells the delegate an ad request failed.
       func adView(_ bannerView: GADBannerView,
           didFailToReceiveAdWithError error: GADRequestError) {
         print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
       }

       /// Tells the delegate that a full-screen view will be presented in response
       /// to the user clicking on an ad.
       func adViewWillPresentScreen(_ bannerView: GADBannerView) {
         print("adViewWillPresentScreen")
       }

       /// Tells the delegate that the full-screen view will be dismissed.
       func adViewWillDismissScreen(_ bannerView: GADBannerView) {
         print("adViewWillDismissScreen")
       }

       /// Tells the delegate that the full-screen view has been dismissed.
       func adViewDidDismissScreen(_ bannerView: GADBannerView) {
         print("adViewDidDismissScreen")
       }

       /// Tells the delegate that a user click will open another app (such as
       /// the App Store), backgrounding the current app.
       func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
         print("adViewWillLeaveApplication")
       
       }
    
    //
    // MARK GOOGLE ADMOB BANNER DELEGATE
    //
    
 
     func rewardedAd(_ rewardedAd: GADRewardedAd, userDidEarn reward: GADAdReward) {
         print("Reward received with currency: \(reward.type), amount \(reward.amount).")
       }
    
    func rewardedAdDidPresent(_ rewardedAd: GADRewardedAd) {
      print("Rewarded ad presented.")
    }

    func rewardedAd(_ rewardedAd: GADRewardedAd, didFailToPresentWithError error: Error) {
      print("Rewarded ad failed to present.")
    }
   
    
    func rewardedAdDidDismiss(_ rewardedAd: GADRewardedAd) {
      print("Rewarded ad dismissed.")
    }
    
    
    

  
    
}

 
