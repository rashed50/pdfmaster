/// Copyright (c) 2020 Razeware LLC
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy
/// of this software and associated documentation files (the "Software"), to deal
/// in the Software without restriction, including without limitation the rights
/// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
/// copies of the Software, and to permit persons to whom the Software is
/// furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in
/// all copies or substantial portions of the Software.
/// 
/// Notwithstanding the foregoing, you may not use, copy, modify, merge, publish,
/// distribute, sublicense, create a derivative work, and/or sell copies of the
/// Software in any work that is designed, intended, or marketed for pedagogical or
/// instructional purposes related to programming, coding, application development,
/// or information technology.  Permission for such use, copying, modification,
/// merger, publication, distribution, sublicensing, creation of derivative works,
/// or sale is expressly withheld.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
/// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
/// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
/// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
/// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
/// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
/// THE SOFTWARE.

import UIKit
import MessageUI


class FileDownloadViewController: UIViewController {

     var allFilesList = [FileInfo]()
  
  
    @IBOutlet weak var progressViewOutlet: UIView!
    @IBOutlet weak var progressViewOutletHeightConstraints: NSLayoutConstraint!
    @IBOutlet weak var FileUrlTextViewOutlet: UITextView!
    
    
    @IBOutlet weak var DownloadProgressOutlet: UIProgressView!
    
    @IBOutlet weak var DownloadProgressTxt: UILabel!
    
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var tableViewBottomConstraint: NSLayoutConstraint!
    
    @IBAction func DownloadBtnAction(_ sender: UIButton) {
       
        Helper.setDocumentDownloadClick(click: true)
      var str : String
        str =  FileUrlTextViewOutlet.text;
        
        if(FileUrlTextViewOutlet.text == nil || FileUrlTextViewOutlet.text == "" || FileUrlTextViewOutlet.text.prefix(4) != "http" || FileUrlTextViewOutlet.text == "Enter Downloadable File(pdf,jpg,png,gif) URL"){
            Helper.showMessageView(title: "Error", message: "Please Enter Valid URL.", view: self)
        }else {
                let prefix = "https"
                let mySubstring = str.prefix(5)
        
                if(mySubstring.lowercased() != prefix){
                    let ss = str.suffix(str.count-4)
                    var pre = prefix
                    pre.append(contentsOf: ss)
                    str = pre
                }
                let furl = URL(string: str)
                var fileName = String()
                fileName = (furl?.absoluteString.components(separatedBy: "/").last)!
                var fileType = String()
                fileType = fileName.components(separatedBy: ".").last!
                let track  = Track.init(name: fileName, artist: fileType, previewURL: furl!, index: 0)
                DownloadProgressOutlet.progress = 0;
                fileDownloadService.startDownload(track)
        }
    }

  
  

  // MARK: - Constants
   //
   
   /// Get local file path: download task stores tune here; AV player plays it.
     let documentsPath = Helper.GetDocumentDirectory(fileName: "")

    // let filePath :String  = Helper.getFileSavingDestinationPath("", fileExtention: "");
    
   let fileDownloadService = FileDownloadService()
   let queryService = QueryService()


 // MARK: - Variables And Properties
  //
  lazy var fileDdownloadsSession: URLSession = {
    let configuration = URLSessionConfiguration.background(withIdentifier:
      "com.raywenderlich.pdfmaster.bgSession")
    return URLSession(configuration: configuration, delegate: self, delegateQueue: nil)
  }()
 
 
  
  lazy var tapRecognizer: UITapGestureRecognizer = {
    var recognizer = UITapGestureRecognizer(target:self, action: #selector(dismissKeyboard))
    return recognizer
  }()
  
  
  
  //
   // MARK: - View Controller
   //
   override func viewDidLoad() {
     super.viewDidLoad()
    
    
     Helper.addBannerInViewToBottom(viewController: self)
    
     self.title = NSLocalizedString("Download", comment: "")
     tableView.tableFooterView = UIView()
     tableView.delegate = self
     tableView.dataSource = self
     FileUrlTextViewOutlet.delegate = self
     fileDownloadService.fileDdownloadsSession = fileDdownloadsSession
     DownloadProgressOutlet.progress = 0;
     setNavigationBarBackButton()
     
     if !Helper.getDocumentDownloadClick(){
        //  FileUrlTextViewOutlet.text = "http://duet.ac.bd/wp-content/uploads/2016/08/CV-Format-for-recruitments.pdf"
          FileUrlTextViewOutlet.text = "https://subratamazumder.starhairbd.com/images/rashed2.jpg"
        // FileUrlTextViewOutlet.text = "https://www.duet.ac.bd/wp-content/uploads/2018/09/Appointment-Notice-2018.doc.pdf"
     }else {
      FileUrlTextViewOutlet.text  = "Enter Downloadable File(pdf,jpg,png,gif) URL"
    }
      
    
      NotificationCenter.default.addObserver(self, selector: #selector(FileDownloadViewController.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
      NotificationCenter.default.addObserver(self, selector: #selector(FileDownloadViewController.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
      self.addDoneButtonOnKeyboard()
      let longPress = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress))
      self.tableView.addGestureRecognizer(longPress)
    
     
   }
    
    

    override func viewWillAppear(_ animated: Bool) {
       super.viewWillAppear(true)
       self.ProgressViewShowHide(height: 0)
       self.SearchAllFileWithReloadTable()
       Helper.setNavigationBarProperty(navbar: self.navigationController!, size: 18, title:NSLocalizedString("Download File", comment: ""))
       
              
    }
  //
  // MARK: - Internal Methods
  //
  @objc func dismissKeyboard() {
    FileUrlTextViewOutlet.resignFirstResponder()
  }
  
  func localFilePath(for url: URL) -> URL {
    return documentsPath.appendingPathComponent(url.lastPathComponent)
  }
  
  func SearchAllFileWithReloadTable() {
   
   allFilesList = Helper.SearchAllDownloadFile()
   self.tableView.reloadData()
  // print(allFilesList.count)
 }
  func position(for bar: UIBarPositioning) -> UIBarPosition {
    return .topAttached
  }
  
  func reload(_ row: Int) {
    tableView.reloadRows(at: [IndexPath(row: row, section: 0)], with: .none)
  }

    
    
    func addDoneButtonOnKeyboard(){
        
        
        let doneToolbar = UIToolbar.init()
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction))
        let items = [flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()

        self.FileUrlTextViewOutlet.inputAccessoryView = doneToolbar
    }

    @objc func doneButtonAction(){
        self.FileUrlTextViewOutlet.resignFirstResponder()
    }
    
 @objc func handleLongPress(sender: UILongPressGestureRecognizer) {

        if sender.state == UIGestureRecognizer.State.began {
            let touchPoint = sender.location(in: tableView)
            if tableView.indexPathForRow(at: touchPoint) != nil {

                let afile :FileInfo = allFilesList[tableView.indexPathForRow(at: touchPoint)!.row]
                Helper.sharePressed(view: self, afile: afile)
                print("Long press Pressed:)")
            }
        }


    }

   @objc  func keyboardWillShow(_ notification: Notification) {
          let userInfo = (notification as NSNotification).userInfo!
    let keyboardSize = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
    self.tableViewBottomConstraint.constant = keyboardSize.size.height-50
   
          
      }

    @objc func keyboardWillHide(_ notification: Notification) {
           self.tableViewBottomConstraint.constant = 0
      }
    
    
    
    func setNavigationBarBackButton() {

          self.navigationItem.setHidesBackButton(true, animated:false)
          let view = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
          let imageView = UIImageView(frame: CGRect(x: 10, y: 10, width: 20, height: 20))
          if let imgBackArrow = UIImage(named: "backBtn") {
              imageView.image = imgBackArrow
            }
          view.addSubview(imageView)
          let backTap = UITapGestureRecognizer(target: self, action: #selector(self.backToMainViewController))
          view.addGestureRecognizer(backTap)
          let leftBarButtonItem = UIBarButtonItem(customView: view)
          self.navigationItem.leftBarButtonItem = leftBarButtonItem
    }
      
    @objc func backToMainViewController() {
          let transition = CATransition()
          transition.duration = 0.4
          transition.type = CATransitionType.push
          transition.subtype = CATransitionSubtype.fromRight
          self.navigationController?.view.layer.add(transition, forKey: kCATransition)
          self.navigationController?.popViewController(animated: false)
                
    }
    
}


 
extension FileDownloadViewController : MFMailComposeViewControllerDelegate{
    
    public func mailComposeController(_ controller: MFMailComposeViewController,
                                       didFinishWith result: MFMailComposeResult,
                                       error: Error?) {
         switch (result) {
         case .cancelled:
             controller.dismiss(animated: true, completion: nil)
         case .sent:
             controller.dismiss(animated: true, completion: nil)
         case .failed:
             controller.dismiss(animated: true, completion: {
                
                 let sendMailErrorAlert = UIAlertController.init(title: "Failed",
                                                                 message: "Unable to send email. Please check your email and Internet Connection " +
                     "settings and try again.", preferredStyle: .alert)
                 sendMailErrorAlert.addAction(UIAlertAction.init(title: "OK",
                                                                 style: .default, handler: nil))
                 controller.present(sendMailErrorAlert, animated: true, completion: nil)
             })
         default:
             break;
         }
     }
}

//
// MARK: - TextView Delegate Method
//
extension FileDownloadViewController: UITextViewDelegate {

    public func textStorage(_ textStorage: NSTextStorage, didProcessEditing editedMask: NSTextStorage.EditActions, range editedRange: NSRange, changeInLength delta: Int) {
        if editedMask.contains(.editedCharacters) {
            
        }
    }
 

func textViewDidBeginEditing(_ textView: UITextView) {
    if(textView.text == "Enter Downloadable File(pdf,jpg,png,gif) URL"){
        
        textView.text = ""
    }
}

func textViewDidEndEditing(_ textView: UITextView) {
     if(textView.text == ""){
         
         textView.text = "Enter Downloadable File(pdf,jpg,png,gif) URL"
     }
}
    
}

//
// MARK: - Table View Data Source
//
extension FileDownloadViewController: UITableViewDataSource {
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell: FileInfoCell = tableView.dequeueReusableCell(withIdentifier:FileInfoCell.identifier,
                                                           for: indexPath) as! FileInfoCell
     
    let afile :FileInfo = allFilesList[indexPath.row]
     
    if(afile.type == PDF_File_TYPE)
    {
          cell.iconImgView.image = UIImage.init(named: "pdf")!
    }else if(afile.type == MSWORD_File_TYPE){
          cell.iconImgView.image = UIImage.init(named: "msword1")!
    }
    else if(afile.type == JPG_IMAGE_File_TYPE || afile.type == "jpeg"  || afile.type == PNG_IMAGE_File_TYPE || afile.type == "gif"){
    
         cell.iconImgView.image = UIImage.init(named: "image")!
    }
    else{
        cell.iconImgView.image = nil;
    }
    
    cell.nameLbl.text =  afile.name
    print(afile.type)

    return cell
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return allFilesList.count
  }
}

//
// MARK: - Table View Delegate
//
extension FileDownloadViewController: UITableViewDelegate {
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
         let afile :FileInfo = allFilesList[indexPath.row]
        Helper.openSelectedFileBy(view: self, afile: afile,navCon: self.navigationController!)
   
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 60
  }
}

 
extension FileDownloadViewController : ReaderViewControllerDelegate {

    func dismiss(_ viewController: ReaderViewController!) {
             print("Long press Pressed:)")
    }  
}

 

//
// MARK: - URL Session Delegate
//
extension FileDownloadViewController: URLSessionDelegate {
  func urlSessionDidFinishEvents(forBackgroundURLSession session: URLSession) {
    DispatchQueue.main.async {
     // if let appDelegate = UIApplication.shared.delegate as? AppDelegate,
      //  let completionHandler = appDelegate.backgroundSessionCompletionHandler {
      //  appDelegate.backgroundSessionCompletionHandler = nil
     //   completionHandler()
     // }
    }
  }
}

//
// MARK: - URL Session Download Delegate
//
extension FileDownloadViewController: URLSessionDownloadDelegate {
  func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask,
                  didFinishDownloadingTo location: URL) {
    // 1
    guard let sourceURL = downloadTask.originalRequest?.url else {
      return
    }
   
    
  //  var da : NSData =   NSData(contentsOf: location, options: NSData.ReadingOptions.Type)
    let download = fileDownloadService.activeDownloads[sourceURL]
    fileDownloadService.activeDownloads[sourceURL] = nil
    
    // 2
    let destinationURL = localFilePath(for: sourceURL)
    print(destinationURL)
    
    // 3
    let fileManager = FileManager.default
   // try? fileManager.removeItem(at: destinationURL)

    do { //destinationURL
      try fileManager.copyItem(at: location, to: destinationURL)
      download?.track.downloaded = true
      self.ProgressViewShowHide(height: 0.0)
  
    } catch let error {
      print("Could not copy file to disk: \(error.localizedDescription)")
    }
     
      DispatchQueue.main.async { [weak self] in
         self?.SearchAllFileWithReloadTable()
       // self?.tableView.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .none)
        
      }
 
  }
 
    
    func ProgressViewShowHide(height: CGFloat) {
         
        DispatchQueue.main.async {
            
            self.progressViewOutletHeightConstraints.constant = height;
            self.progressViewOutlet.layoutIfNeeded()
        }
    }
  
  func updateProgressDisplay(progress: Float, totalSize : String) {
    
    self.ProgressViewShowHide(height: 20.0)
    
    DownloadProgressOutlet.progress = progress
    DownloadProgressTxt.text = String(format: "%.1f%% of %@", progress * 100, totalSize)
  }
  
  func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask,
                  didWriteData bytesWritten: Int64, totalBytesWritten: Int64,
                  totalBytesExpectedToWrite: Int64) {
    // 1
    guard
      let url = downloadTask.originalRequest?.url,
      let download = fileDownloadService.activeDownloads[url]  else {
        return
    }
    print("total byte \(totalBytesExpectedToWrite)")
    // 2
    download.progress = Float(totalBytesWritten) / Float(totalBytesExpectedToWrite)
    // 3
    let totalSize = ByteCountFormatter.string(fromByteCount: totalBytesExpectedToWrite, countStyle: .file)
   // https://duet.ac.bd/wp-content/uploads/2016/08/CV-Format-for-recruitments.pdf
    // 4
    DispatchQueue.main.async {
      
      self.updateProgressDisplay(progress: download.progress, totalSize: totalSize)
      print("download end")
      /*
       if let trackCell = self.tableView.cellForRow(at: IndexPath(row: download.track.index,
                                                                 section: 0)) as? TrackCell {
         trackCell.updateDisplay(progress: download.progress, totalSize: totalSize)
      }
     */
    }
  }
}

 
