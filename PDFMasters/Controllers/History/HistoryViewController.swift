//
//  HistoryViewController.swift
//  Doc Scanner
//
//  Created by Rashed on 2017-01-17.
//  Copyright © 2017 iRLMobile. All rights reserved.
//

import UIKit
import MessageUI


class HistoryViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    
    var allDocumentInfoDic :NSMutableDictionary = NSMutableDictionary()
    
  
    let cellReuseIdentifier = "cellInfo"
    
    // don't forget to hook this up from the storyboard
    
    @IBOutlet var documentListTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Helper.addBannerInViewToBottom(viewController: self)
        setNavigationBarBackButton()
        documentListTableView.delegate = self
        documentListTableView.dataSource = self
        documentListTableView.sectionHeaderHeight = 30
        documentListTableView.estimatedSectionHeaderHeight = UITableView.automaticDimension
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress))
          self.documentListTableView.addGestureRecognizer(longPress)
        self.title = NSLocalizedString("File Manager" , comment: "")
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        Helper.setNavigationBarProperty(navbar: self.navigationController!, size: 18, title:NSLocalizedString("File Manager", comment: ""))
        allDocumentInfoDic =  Helper.getAllFile()
        self.documentListTableView.reloadData()
      
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()        
    }
    
    
    func setNavigationBarBackButton() {

           self.navigationItem.setHidesBackButton(true, animated:false)
           let view = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
           let imageView = UIImageView(frame: CGRect(x: 10, y: 10, width: 20, height: 20))
           if let imgBackArrow = UIImage(named: "backBtn") {
               imageView.image = imgBackArrow
             }
           view.addSubview(imageView)
           let backTap = UITapGestureRecognizer(target: self, action: #selector(self.backToMainViewController))
           view.addGestureRecognizer(backTap)
           let leftBarButtonItem = UIBarButtonItem(customView: view)
           self.navigationItem.leftBarButtonItem = leftBarButtonItem
     }
       
     @objc func backToMainViewController() {
           let transition = CATransition()
           transition.duration = 0.4
           transition.type = CATransitionType.push
           transition.subtype = CATransitionSubtype.fromRight
           self.navigationController?.view.layer.add(transition, forKey: kCATransition)
           self.navigationController?.popViewController(animated: false)
                 
     }
    
/// MARK TABLE VIEW DELEGATE METHODS
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:UITableViewCell = (self.documentListTableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier))!
        
        
        let allSectionList :NSArray = allDocumentInfoDic.allKeys as NSArray
        var sectionTitle = ""
        sectionTitle = allSectionList[indexPath.section] as! String;
        
        let listData =  (allDocumentInfoDic.value(forKey: sectionTitle)) as! NSArray
        
        
         let iconView :UIImageView = cell.viewWithTag(10) as! UIImageView
         let nameLbl :UILabel = cell.viewWithTag(11) as! UILabel
        nameLbl.text = listData[indexPath.row] as? String
       // iconView.image = UIImage.init(named: "")
        
        let afile = (listData[indexPath.row] as? String)?.split(separator: ".").last ?? "pdf"
        let ty : String = (String(afile) as String)
        
           if(ty == PDF_File_TYPE)
           {
                  iconView.image = UIImage.init(named: "pdf")!
           }else if(ty == MSWORD_File_TYPE){
                 iconView.image = UIImage.init(named: "msword.png")!
           }
          else if(ty == TEXT_File_TYPE){
                  iconView.image = UIImage.init(named: "text.png")!
            }
           else if(ty == JPG_IMAGE_File_TYPE || ty == "jpeg"  || ty == PNG_IMAGE_File_TYPE || ty == "gif"){
            
                 iconView.image = UIImage.init(named: "image")!
            }
           else{
               iconView.image = nil;
           }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
         let allSectionList :NSArray = allDocumentInfoDic.allKeys as NSArray
         return allSectionList[section] as? String
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        let allSectionList :NSArray = allDocumentInfoDic.allKeys as NSArray
        return allSectionList.count;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        let allSectionList :NSArray = allDocumentInfoDic.allKeys as NSArray
        var sectionTitle = ""
        sectionTitle = allSectionList[section] as! String;
        
        let listData =  (allDocumentInfoDic.value(forKey: sectionTitle)) as! NSArray
        
        return  listData.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        /*
        NSString *filePath =[allBookPathArray objectAtIndex:indexPath.row];
        
        NSString *phrase = nil;
        ReaderDocument *document = [ReaderDocument withDocumentFilePath:filePath password:phrase];
        
        if (document != nil)
        {
            ReaderViewController *readerViewController = [[ReaderViewController alloc] initWithReaderDocument:document];
            
            readerViewController.delegate = self;
            
            #if (DEMO_VIEW_CONTROLLER_PUSH == TRUE)
                
                [self.navigationController pushViewController:readerViewController animated:YES];
                
            #else // present in a modal view controller
                
                readerViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
                readerViewController.modalPresentationStyle = UIModalPresentationFullScreen;
                
                [self presentViewController:readerViewController animated:YES completion:NULL];
                
            #endif // DEMO_VIEW_CONTROLLER_PUSH
        }
        else // Log an error so that we know that something went wrong
        {
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Error !" message:@"May be you have no access permission. or Fil is not exists " delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            [alertView show];
            NSLog(@"%s [ReaderDocument withDocumentFilePath:'%@' password:'%@'] failed.", __FUNCTION__, filePath, phrase);
        }
        
        */
        
        let allSectionList :NSArray = allDocumentInfoDic.allKeys as NSArray
        var sectionTitle = ""
        sectionTitle = allSectionList[indexPath.section] as! String;
        
        let listData =  (allDocumentInfoDic.value(forKey: sectionTitle)) as! NSArray
        
        var filePath : String = String(listData[indexPath.row] as! String)
        filePath =  Helper.getFilePathWithExtention("\(sectionTitle)/\(filePath)")
        
        let ( fname,type,url, afile) = Helper.getFileNameAndTypeFromFilePath(path: filePath)
        print(filePath)
         
        Helper.openSelectedFileBy(view: self, afile: afile,navCon: self.navigationController!)
        
        
    }
    
  // MARK ReaderViewController Delegate Methods

    //  - (void)dismissReaderViewController:(ReaderViewController *)viewController;
   
    
  @objc func handleLongPress(sender: UILongPressGestureRecognizer) {

      if sender.state == UIGestureRecognizer.State.began {
          let touchPoint = sender.location(in: documentListTableView)
          if documentListTableView.indexPathForRow(at: touchPoint) != nil {

            let indexpath : IndexPath = documentListTableView.indexPathForRow(at: touchPoint)!
            
            let allSectionList :NSArray = allDocumentInfoDic.allKeys as NSArray
                   var sectionTitle = ""
                   sectionTitle = allSectionList[indexpath.section] as! String;
                   
                   let listData =  (allDocumentInfoDic.value(forKey: sectionTitle)) as! NSArray
                   
                   var filePath : String = String(listData[indexpath.row] as! String)
                   filePath =  Helper.getFilePathWithExtention("\(sectionTitle)/\(filePath)")
            
             // let afile :FileInfo = allFilesList[documentListTableView.indexPathForRow(at: touchPoint)!.row]
           // Helper.sharePressed(view: self, fileurl: filePath.url)
            
            let ( fname,type,url, afile) = Helper.getFileNameAndTypeFromFilePath(path: filePath)
                   print(filePath)
            
            Helper.sharePressed(view: self, afile: afile)
              print("Long press Pressed:)")
              
              
          }
      }


  }
  
    
}
 
 extension HistoryViewController : ReaderViewControllerDelegate {

     func dismiss(_ viewController: ReaderViewController!) {
              print("Long press Pressed:)")
     }
 }
extension HistoryViewController : MFMailComposeViewControllerDelegate{
    
    public func mailComposeController(_ controller: MFMailComposeViewController,
                                       didFinishWith result: MFMailComposeResult,
                                       error: Error?) {
         switch (result) {
         case .cancelled:
             controller.dismiss(animated: true, completion: nil)
         case .sent:
             controller.dismiss(animated: true, completion: nil)
         case .failed:
             controller.dismiss(animated: true, completion: {
                
                 let sendMailErrorAlert = UIAlertController.init(title: NSLocalizedString("Error", comment: ""),
                                                                 message: "Unable to send email. Please check your email and Internet Connection " +
                     "settings and try again.", preferredStyle: .alert)
                 sendMailErrorAlert.addAction(UIAlertAction.init(title: NSLocalizedString("OK", comment: ""),
                                                                 style: .default, handler: nil))
                 controller.present(sendMailErrorAlert, animated: true, completion: nil)
             })
         default:
             break;
         }
     }
}
