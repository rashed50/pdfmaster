//
//  LeftMenuViewController.swift
//  SlideMenu
//
//  Created by Rashedul Hoque on 19/6/20.
//  Copyright © 2020 Rashedul Hoque. All rights reserved.
//

import UIKit
 

class LeftMenuViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

      
     let menuList = ["Download File","Scan Analog File", "File Manager", "Refer a Friend", "Developer App", "Send Feedback"]
     let cellReuseIdentifier = "cell"
    
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        tableView.register(UINib(nibName: "MenuTableCell", bundle: nil), forCellReuseIdentifier: cellReuseIdentifier)
 
    }

    // MARK: - Table view data source

     func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return menuList.count
    }

  
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
       // let identifier = "Cell"
        var celll: MenuTableCell! = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as? MenuTableCell
        if celll == nil {
            celll = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as? MenuTableCell
        }
        print(menuList.count)
        print(indexPath.row)
       
         celll.menuTitleLabel!.text = menuList[indexPath.row]
         celll.menuImageView!.image = UIImage.init(named: String.init(format: "menu_%d", indexPath.row))
         celll.menuImageView.contentMode = .scaleAspectFit
         return celll
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         
   
        if indexPath.row == 0 {
            // Download
            let fileDnload = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "fileDownloadViewController")
            self.navigationController?.pushViewController(fileDnload, animated: true)
            
        }else if(indexPath.row == 1){
             // Scan
            let story = UIStoryboard.init(name: "Main", bundle: nil)
            var viewCon : UIViewController
            if #available(iOS 13.0, *) {
                viewCon = story.instantiateViewController(identifier: "scan2SideViewController")
            } else {
                viewCon = story.instantiateViewController(withIdentifier: "scan2SideViewController")
            }
            self.navigationController?.pushViewController(viewCon, animated: true)
        }
        else if indexPath.row == 2{
                // ALL Files
           let story = UIStoryboard.init(name: "Main", bundle: nil)
           var viewCon : UIViewController
           if #available(iOS 13.0, *) {
               viewCon = story.instantiateViewController(identifier: "historyViewController")
           } else {
               viewCon = story.instantiateViewController(withIdentifier: "historyViewController")
           }
           self.navigationController?.pushViewController(viewCon, animated: true)
                 
        }
        else if indexPath.row == 3{
            // Refer a Friend
            Helper.sendEmail(viewCon: self, fileUrl:nil ,operationType: 2)
        }
        else if indexPath.row == 4{
            // Developer APP
            let story = UIStoryboard.init(name: "MyAppMain", bundle: nil)
            var viewCon : UIViewController
            if #available(iOS 13.0, *) {
                  viewCon = story.instantiateViewController(identifier: "pageContainerViewController")
            } else {
               viewCon = story.instantiateViewController(withIdentifier: "pageContainerViewController")
            }
            
            self.navigationController?.pushViewController(viewCon, animated: true)
         }
        else if indexPath.row == 5{
            // Feedback & suggestions
             Helper.sendEmail(viewCon: self, fileUrl:nil ,operationType: 1)
        }
        
        print("testing row is seleected")
    }
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
         
         print("testing row is seleected")
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
         
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 120))

        let label = UILabel()
        label.frame = CGRect.init(x: headerView.frame.width/2-60, y: headerView.frame.height/2-60, width: 120, height: 120)
        label.text = Helper.getAppName()
        label.textAlignment = .center
        label.backgroundColor = Helper.getAppColor()
        label.layer.cornerRadius = label.frame.width/2
        label.layer.masksToBounds = true
      // label.font = UIFont().futuraPTMediumFont(16) // my custom font
        label.textColor = UIColor.white // my custom colour
        headerView.addSubview(label)

        return headerView
    }
  
     func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 120
    }
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }

}



