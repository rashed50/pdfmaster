//
//  ViewController.swift
//  SlideMenu
//
//  Created by Rashedul Hoque on 19/6/20.
//  Copyright © 2020 Rashedul Hoque. All rights reserved.
//

import UIKit
import SideMenu
import BSImagePicker
import Photos


class MainViewController: UIViewController {

    
    @IBOutlet weak var adBannerView: UIView!
    @IBOutlet weak var pdfFromGalleryButton: UIButton!
    @IBOutlet weak var scanAnalogFileButton: UIButton!

    @IBOutlet weak var pdfFromGalleryButton1: UIButton!
    @IBOutlet weak var scanAnalogFileButton1: UIButton!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        
        Helper.SetButtonUIProperty(button: pdfFromGalleryButton, title: "", fontSize: 20)
        Helper.SetButtonUIProperty(button: scanAnalogFileButton, title: "", fontSize: 20)
        Helper.SetButtonUIProperty(button: pdfFromGalleryButton1, title: "", fontSize: 20)
        Helper.SetButtonUIProperty(button: scanAnalogFileButton1, title: "", fontSize: 20)
        // Override background color
        pdfFromGalleryButton.backgroundColor = UIColor.systemGray
        scanAnalogFileButton.backgroundColor = UIColor.systemGray
        pdfFromGalleryButton1.backgroundColor = UIColor.systemGray
        scanAnalogFileButton1.backgroundColor = UIColor.systemGray
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.scanFileSaveCompeted(notification:)), name: Notification.Name("2slidescanfilesavecompleted"), object: nil)
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Helper.setNavigationBarProperty(navbar: self.navigationController, size: 18, title:Helper.getAppName())
        Helper.addBannerInViewToBottom(viewController: self)
    }

    @IBAction func scanAnalogFile(_ sender: Any) {
        
        let story = UIStoryboard.init(name: "Main", bundle: nil)
        var viewCon : UIViewController
        if #available(iOS 13.0, *) {
            viewCon = story.instantiateViewController(identifier: "scan2SideViewController")
        } else {
            viewCon = story.instantiateViewController(withIdentifier: "scan2SideViewController")
        }
        self.navigationController?.pushViewController(viewCon, animated: true)
    }
    
    @IBAction func openPhotoGalleryAction(_ sender: Any) {
        let imagePicker = ImagePickerController()
        presentImagePicker(imagePicker, select: { (asset) in
            // User selected an asset. Do something with it. Perhaps begin processing/upload?
        }, deselect: { (asset) in
            // User deselected an asset. Cancel whatever you did when asset was selected.
        }, cancel: { (assets) in
            // User canceled selection.
        }, finish: { (assets) in
           
            self.dismiss(animated: true, completion: nil)
            self.showSpinner(onView: self.view)
             
            var allImage  = [UIImage]()
            let requestOptions = PHImageRequestOptions()
            requestOptions.resizeMode = PHImageRequestOptionsResizeMode.exact
            requestOptions.deliveryMode = PHImageRequestOptionsDeliveryMode.highQualityFormat
               // this one is key
            requestOptions.isSynchronous = true

               for asset in assets
               {
                   if (asset.mediaType == PHAssetMediaType.image)
                   {

                    PHImageManager.default().requestImage(for: asset , targetSize: PHImageManagerMaximumSize, contentMode: PHImageContentMode.default, options: requestOptions, resultHandler: { (pickedImage, info) in
                        if pickedImage != nil {
                            allImage.append(pickedImage!)
                        }

                       })

                   }
               }
            self.removeSpinner()
            print(allImage.count)
            Helper.showFileNameAlertAndSaveDocument(viewCon: self, operationType: 2, saveAsFormat: "pdf", fileInfo: allImage)
             
        })
    }
    
   
    
    
    @objc func scanFileSaveCompeted(notification: Notification) {
           self.gotoScanHistory()
       }
       
       func gotoScanHistory() {
                 let story = UIStoryboard.init(name: "Main", bundle: nil)
                var viewCon : UIViewController
                if #available(iOS 13.0, *) {
                    viewCon = story.instantiateViewController(identifier: "historyViewController")
                } else {
                    viewCon = story.instantiateViewController(withIdentifier: "historyViewController")
                }
                self.navigationController?.pushViewController(viewCon, animated: true)
                              
          }
    
}

extension MainViewController: SideMenuNavigationControllerDelegate {

    func sideMenuWillAppear(menu: SideMenuNavigationController, animated: Bool) {
        print("SideMenu Appearing! (animated: \(animated))")
    }

    func sideMenuDidAppear(menu: SideMenuNavigationController, animated: Bool) {
        print("SideMenu Appeared! (animated: \(animated))")
    }

    func sideMenuWillDisappear(menu: SideMenuNavigationController, animated: Bool) {
        print("SideMenu Disappearing! (animated: \(animated))")
    }

    func sideMenuDidDisappear(menu: SideMenuNavigationController, animated: Bool) {
        print("SideMenu Disappeared! (animated: \(animated))")
    }
    
    
}


var vSpinner : UIView?
extension UIViewController {
    func showSpinner(onView : UIView) {
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        let ai = UIActivityIndicatorView.init(style: .whiteLarge)
        ai.startAnimating()
        ai.center = spinnerView.center
        
        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            onView.addSubview(spinnerView)
        }
        
        vSpinner = spinnerView
    }
    
    func removeSpinner() {
        DispatchQueue.main.async {
            vSpinner?.removeFromSuperview()
            vSpinner = nil
        }
    }
}
