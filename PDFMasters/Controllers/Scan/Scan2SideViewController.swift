//
//  ViewController.swift
//  MBDocCapture
//
//  Created by El Mahdi Boukhris on 16/04/2019.
//  Copyright © 2019 El Mahdi Boukhris <m.boukhris@gmail.com>
//

import UIKit
import MBDocCapture

class Scan2SideViewController: UIViewController {

    @IBOutlet weak var resultContainerView: UIView!
    @IBOutlet weak var page1Preview: UIImageView!
    @IBOutlet weak var page2Preview: UIImageView!
    
    @IBOutlet weak var startScanButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Helper.addBannerInViewToBottom(viewController: self)
        setNavigationBarBackButton()
        saveButton.isHidden = true
        Helper.SetButtonUIProperty(button: saveButton, title: "", fontSize: 18)
        Helper.SetButtonUIProperty(button: startScanButton, title: "", fontSize: 18)
        NotificationCenter.default.addObserver(self, selector: #selector(self.scanFileSaveCompeted(notification:)), name: Notification.Name("2slidescanfilesavecompleted"), object: nil)
       

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        Helper.setNavigationBarProperty(navbar: self.navigationController!, size: 18, title: "Scan Analog Documents")
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationItem.title = "Scan Analog Documents"
        self.tabBarController?.tabBar.isHidden = true
      }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func didSelectType1Button(_ sender: Any) {
        let scanner = ImageScannerController(delegate: self)
        scanner.shouldScanTwoFaces = false
        present(scanner, animated: true)
    }
    
    //startScanning2SidesDoc
    @IBAction func didSelectType2Button(_ sender: Any) {
        let scanner = ImageScannerController(delegate: self)
        scanner.shouldScanTwoFaces = true
        present(scanner, animated: true)
    }
    
    
    
    @IBAction func SaveAction(_ sender: Any) {
        
        let imagList  = [self.page1Preview.image!,self.page2Preview.image!]
        Helper.showFileNameAlertAndSaveDocument(viewCon: self, operationType: 2, saveAsFormat: "pdf", fileInfo: imagList)
       
        
    }
    
    
    
    @IBAction func didSelectPreview1Button(_ sender: Any) {
        let scanner = ImageScannerController(image: page1Preview.image, delegate: self)
        present(scanner, animated: true)
    }
    
    @IBAction func didSelectPreview2Button(_ sender: Any) {
        let scanner = ImageScannerController(image: page2Preview.image, delegate: self)
        present(scanner, animated: true)
    }
   

    func setNavigationBarBackButton() {

           self.navigationItem.setHidesBackButton(true, animated:false)
           let view = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
           let imageView = UIImageView(frame: CGRect(x: 10, y: 10, width: 20, height: 20))
           if let imgBackArrow = UIImage(named: "backBtn") {
               imageView.image = imgBackArrow
             }
           view.addSubview(imageView)
           let backTap = UITapGestureRecognizer(target: self, action: #selector(self.backToMainViewController))
           view.addGestureRecognizer(backTap)
           let leftBarButtonItem = UIBarButtonItem(customView: view)
           self.navigationItem.leftBarButtonItem = leftBarButtonItem
     }
       
     @objc func backToMainViewController() {
           let transition = CATransition()
           transition.duration = 0.4
           transition.type = CATransitionType.push
           transition.subtype = CATransitionSubtype.fromRight
           self.navigationController?.view.layer.add(transition, forKey: kCATransition)
           self.navigationController?.popViewController(animated: false)
                 
     }
    
//
// MARK NOTIFICATION OVSERVERTION METHOD
//
 
    @objc func scanFileSaveCompeted(notification: Notification) {
        self.page1Preview.image = nil
        self.page2Preview.image = nil
        saveButton.isHidden = true
        self.resultContainerView.isHidden = true
        self.gotoScanHistory()
    }
    
    func gotoScanHistory() {
             print(self.navigationController)
             let story = UIStoryboard.init(name: "Main", bundle: nil)
             var viewCon : UIViewController
             if #available(iOS 13.0, *) {
                 viewCon = story.instantiateViewController(identifier: "historyViewController")
             } else {
                 viewCon = story.instantiateViewController(withIdentifier: "historyViewController")
             }
             self.navigationController?.pushViewController(viewCon, animated: true)
                           
       }
    
    
}


//
// MARK ImageScannerControllerDelegate
//

extension Scan2SideViewController: ImageScannerControllerDelegate {
    func imageScannerController(_ scanner: ImageScannerController, didFinishScanningWithResults results: ImageScannerResults) {
        scanner.dismiss(animated: true) {
           
            self.resultContainerView.isHidden    =   false
            self.page2Preview.isHidden           =   true
            
            if results.doesUserPreferEnhancedImage {
                self.page1Preview.image          =   results.enhancedImage
            } else {
                self.page1Preview.image          =   results.scannedImage
            }
        }
    }
    
    func imageScannerController(_ scanner: ImageScannerController, didFinishScanningWithPage1Results page1Results: ImageScannerResults, andPage2Results page2Results: ImageScannerResults) {
        scanner.dismiss(animated: true) {
            self.saveButton.isHidden             =   false
            self.resultContainerView.isHidden    =   false
            self.page2Preview.isHidden           =   false
            
            if page1Results.doesUserPreferEnhancedImage {
                self.page1Preview.image          =   page1Results.enhancedImage
            } else {
                self.page1Preview.image          =   page1Results.scannedImage
            }
            
            if page2Results.doesUserPreferEnhancedImage {
                self.page2Preview.image          =   page2Results.enhancedImage
            } else {
                self.page2Preview.image          =   page2Results.scannedImage
            }
        }
    }
    
    func imageScannerControllerDidCancel(_ scanner: ImageScannerController) {
        scanner.dismiss(animated: true)
    }
    
    func imageScannerController(_ scanner: ImageScannerController, didFailWithError error: Error) {
        scanner.dismiss(animated: true)
    }
}
