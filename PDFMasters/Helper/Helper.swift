//
//  Helper.swift
//  Doc Scanner
//
//  Created by Rashed on 2017-01-19.
//  Copyright © 2017 iRLMobile. All rights reserved.
//

import UIKit
import MessageUI
//import PDFReader
import PDFGenerator
 


let APP_HEXA_COlOR_CODE = "234234"
let APP_TEXT_HEXA_COlOR_CODE = "ffffff"

let  APP_NAME  = "PDF Generator"
let DOWNLOAD_File_NAME  = "pdfgenerator"
let PDF_File_TYPE   = "pdf"
let MSWORD_File_TYPE   = "doc"
let MSWORD_File_TYPE10   = "docx"
let TEXT_File_TYPE   = "txt"
let MSEXCEL_File_TYPE   = "xls"
let MSEXCEL_File_TYPE10   = "xlsx"
let PNG_IMAGE_File_TYPE   = "png"
let JPG_IMAGE_File_TYPE   = "jpg"
let JPEG_IMAGE_File_TYPE   = "jpeg"
let GIF_IMAGE_File_TYPE   = "gif"

let directoryName : String = "AllDocuments"
let tempDirectoryName : String = "tempDocuments"
let tempOCRDirectoryName : String = "ocrTempDocuments"

@objc
class Helper: NSObject {
   
     
    
    @objc static func showFileNameAlertAndSaveDocument(viewCon:UIViewController,operationType:Int,saveAsFormat:String,fileInfo:[UIImage]?)  {
                   //Step : 1
                   let alert = UIAlertController(title: "File Name", message: "", preferredStyle: UIAlertController.Style.alert )
                   //Step : 2
                   let save = UIAlertAction(title: "Save", style: .default) { (alertAction) in
                       
                           let textField = alert.textFields![0] as UITextField
                           var fname : String =  textField.text ?? ""
                           if textField.text == "" {
                               fname = Helper.getTodayDate()
                           }
                           if(operationType == 1) // OCR Info Save
                           {
                              // _ = Helper.saveOCRResult(ocrResultStr: self.ocrTextView.text, fileName: fname , fileType: saveAsFormat )
                              
                           }
                           else if(operationType == 2) // save 2 side scan file
                           {
                              
                               let filePath :String  = self.getFileSavingDestinationPath(fname)
                                      
                                      do {
                                          let pass = PDFPassword.init("")  //self.getPdfDocumentPassword()
                                       let pdfData = try PDFGenerator.generated(by: fileInfo!, dpi:.default, password:pass)
                                          try pdfData.write(to: URL(fileURLWithPath: filePath) )
                                          //print("PDF file destination \(filePath)");
                                        NotificationCenter.default.post(name: Notification.Name("2slidescanfilesavecompleted"), object: nil)
                                      }
                                      catch (let e) {
                                          //print("pdf data wirting error is %@",e)
                                          self.showMessageView(title: "Error", message: "File Didn't Save", view: viewCon)
                                        
                                      }
                                
                           }
                       }

                   alert.addTextField { (textField) in
                       textField.placeholder = "Enter File Name"
                       textField.textColor = .black
                   }
                   alert.addAction(save)
                   alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { (alertAction) in
                   })
                    
             viewCon.present(alert, animated:true, completion: nil)
                    
       }
    
    static func showMessageView(title: String, message: String, view: UIViewController){
           
                       let alert = UIAlertController(title: title , message: message , preferredStyle: .alert)
                                      alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                            
                                      }))
                       view.present(alert, animated: true, completion: nil)
           
       }
        
    
    static func openSelectedFileBy(view: UIViewController, afile: FileInfo, navCon: UINavigationController) {
        
                      var filepath :String =   String(afile.url.path)
                       print(filepath)
                       
                        if(afile.type == "pdf"){
                               if(FileManager.default.isReadableFile(atPath: filepath)){
                                let dd = NSData.init(contentsOfFile: filepath)
                                if(dd == nil)
                                { filepath = ""}
                               }
                               else {filepath = "" }
                        }
                        else //if(afile.type != PDF_File_TYPE)
                        {
                            filepath =  self.temporarySaveFileForDisplayInPDFViewer(afile: afile)
                        }
                       
                       if(filepath != ""){
                        
                       // let documentFileURL = Bundle.main.url(forResource: "Cupcakes", withExtension: "pdf")!
                     //   let document = PDFDocument(url: URL.init(fileURLWithPath: filepath))!
                        
                      //  let readerController = PDFViewController.createNew(with: document)
                      //  navCon.pushViewController(readerController, animated: true)
                     //   view.present(readerController, animated: true, completion: nil)
                         
                        
                        let document : ReaderDocument? = ReaderDocument.init(filePath: filepath, password:self.getPdfDocumentPassword())
                              if(document != nil){
                                    let docReaderController : ReaderViewController = ReaderViewController.init(readerDocument:document)
                                    docReaderController.delegate = view as? ReaderViewControllerDelegate
                                
                                  DispatchQueue.main.async {
                                                view.present(docReaderController, animated: true, completion: {
                                                 
                                                 })
                                    }
                                    
                                }
                              else
                              {
                                self.showMessageView(title: "Error", message: "Password or File Path Error", view: view)
                               }
                        
                        
                           }
                        
                       else //if()
                       {
                           self.showMessageView(title: "Error", message: "Corrupted File", view: view)
                        }
          
    
    }
       static func sharePressed(view: UIViewController, afile: FileInfo)
        {
           let alertController = UIAlertController(title: NSLocalizedString("Choose", comment: ""), message: "", preferredStyle: .actionSheet)
           let fileurl = afile.url
          
           let action2 = UIAlertAction(title: NSLocalizedString("File Delete", comment: ""), style: .default) { (action) in
               
                   do {
                       try FileManager.default.removeItem(atPath: fileurl.path)
                        
                      } catch (let er) {
                          print(er)
                      }
           }
           let action3 = UIAlertAction(title: "Open & Share", style: .default) { (action) in
                 print("Default is pressed.....")
                self.openSelectedFileBy(view: view, afile: afile, navCon: view.navigationController!)
             }
           let action5 = UIAlertAction(title: "Email", style: .default) { (action) in
                 self.sendEmail(viewCon:view, fileUrl: fileurl ,operationType: 0)
             }
           let action6 = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel) { (action) in
           }
           
           
          
           
           
         //  if(afile.type == TEXT_File_TYPE){
           // alertController.addAction(action1)
            alertController.addAction(action2)
            alertController.addAction(action3)
           // alertController.addAction(action4)
            alertController.addAction(action5)
            alertController.addAction(action6)
          // }
          // else {
          //     alertController.addAction(action2)
          //     alertController.addAction(action6)
          // }
           if let popoverController = alertController.popoverPresentationController {
                   popoverController.sourceView = view.view
                   popoverController.sourceRect = CGRect(x: view.view.bounds.midX, y: view.view.bounds.midY, width: 0, height: 0)
                   popoverController.permittedArrowDirections = []
            }
            view.present(alertController, animated: true, completion: nil)
    }
 
    
    //
    // MARK DIRECTORY CREATE AND OPERATION
    //
    
    @objc
     static func createDirectory() {
         
         do {
             try FileManager.default.createDirectory(atPath:self.getBasePath(basePath: tempDirectoryName), withIntermediateDirectories: false, attributes: nil)
             try FileManager.default.createDirectory(atPath:self.getBasePath(basePath: directoryName), withIntermediateDirectories: false, attributes: nil)
           //  try FileManager.default.createDirectory(atPath:self.getBasePath(basePath: tempOCRDirectoryName), withIntermediateDirectories: false, attributes: nil)
             
             
             
         } catch let error as NSError {
             print(error.localizedDescription);
             
         }
         
     }
    
    @objc
      static  func getTodayDate( ) -> String {
          let date = Date()
          let formatter = DateFormatter()
          formatter.dateFormat = "dd.MM.YYYY"
          let  todayDateStr = formatter.string(from: date)
          return todayDateStr.isEmpty ? "" : todayDateStr
      }
    
    static func temporarySaveFileForDisplayInPDFViewer(afile: FileInfo) -> String {
           
           let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
           let fileNewPath = "\(documentsPath)/\(tempDirectoryName)/pdfdisplay.pdf"
           
                let filePath = afile.url.path
                if FileManager.default.fileExists(atPath: filePath) {
                   do {
                       
                       if(afile.type == JPG_IMAGE_File_TYPE || afile.type == "jpeg"  || afile.type == PNG_IMAGE_File_TYPE || afile.type == "gif"){
                           try PDFGenerator.generate(filePath, to: fileNewPath)
                           return fileNewPath
                         }
                        }
                        catch (let e) {
                              print("pdf data wirting error is %@",e)
                              }
                   }
           
           return ""
       }
    static func getFileNameAndTypeFromFilePath(path:String)-> (fname: String, type: String, fileurl: URL, afile: FileInfo)  {
        
                 var spitlist = [String]()
                   spitlist = path.components(separatedBy:"/")
                   var type = String()
                   type = spitlist.last!.components(separatedBy: ".").last!
                  let url : URL = URL.init(fileURLWithPath: path)
                    let afile = FileInfo.init(name: spitlist.last!, type: type, url:url )
        return (spitlist.last!,type,url,afile)
        
        
    }
    
    @objc
       static func getAllDirectores() -> NSMutableArray {
           
           let allDirectoryList : NSMutableArray = []
           do {
               let dirList = try FileManager.default.contentsOfDirectory(atPath: self.getBasePath(basePath: directoryName))
               
               for abc in dirList {
                   allDirectoryList.add(abc)
               }
           }catch let error as NSError {
               print("error at getAllDirectory() ==>> \(error.localizedDescription)");
           }
           return allDirectoryList
       }
    
    @objc
      static func getAllFile() -> NSMutableDictionary {
          
          let listDictionaryData :NSMutableDictionary = NSMutableDictionary()
          
          var allDirList = NSMutableArray()
          allDirList = self.getAllDirectores()
          
          for folder in allDirList {
              let folderName = String(folder as! String)
              var basePath = "";
              basePath = basePath.appendingFormat("%@/%@", directoryName,folder as! String)
              basePath = self.getBasePath(basePath: basePath)
              
              do {
                  let fileList = try FileManager.default.contentsOfDirectory(atPath: basePath)
                  
                  if fileList.count > 0
                  {
                      listDictionaryData.setObject(fileList, forKey: folderName as NSCopying)
                  }
                  
              }catch {
                  
              }
          }
          return listDictionaryData
      }
    
     @objc
    static func getFilePathWithExtention(_ fileName: String) -> String {
        let basePath = self.getBasePath(basePath: directoryName)
        let filePath = "\(basePath)/\(fileName)"
        return filePath
    }
    @objc
       static func createTodayFolder() -> String {
           
           var basePath = "";
           basePath = basePath.appendingFormat("%@/%@", directoryName,getTodayDate()) //getTodayDate()
           basePath = self.getBasePath(basePath: basePath)
           
           do {
               
               try FileManager.default.createDirectory(atPath:basePath, withIntermediateDirectories: false, attributes: nil)
               print("success")
               
           } catch let error as NSError {
               print(error.localizedDescription);
               
           }
           return basePath;
           
       }
    static func getFileSavingDestinationPath(_ fileName: String) -> String {
           
          // let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
           let basePath = self.createTodayFolder()
           let filePath = "\(basePath)/\(fileName).pdf"
           return filePath
       }
     @objc
    static func getTemporaryFileDestinationPath(_ fileName: String ,imgFile:UIImage) -> String {
        
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let filePath = "\(documentsPath)/\(tempDirectoryName)/\(fileName).png"
        
        let imgdata :Data = imgFile.jpegData(compressionQuality: 1)!
        
        do {
            try imgdata.write(to: URL(fileURLWithPath: filePath) )
            print("file temp destination \(filePath)");
        }
        catch (let e) {
            print("data wirting error is %@",e)
        }
        return filePath
    }
    
    static  func SearchAllDownloadFile() -> [FileInfo] {
         
         var allList = [URL]()
         var allFileList = [FileInfo]()

         do {
             allList = try FileManager.default.contentsOfDirectory(at: GetDocumentDirectory(fileName: ""), includingPropertiesForKeys: nil, options:.skipsHiddenFiles)
           print("all \(allList)")
           for aa in allList {
             var spitlist = [String]()
             spitlist = aa.absoluteString.components(separatedBy:"/")
               var type = String()
               type = spitlist.last!.components(separatedBy: ".").last!
               if(spitlist.last! != "" ) {
                   let f = FileInfo.init(name: spitlist.last!, type: type, url: aa)
                   allFileList.append(f)
               }
           }
         
         } catch  {
           print("Error")
         }
         
    
         
         
         return allFileList
       }
    
    
    
    static func GetDocumentDirectory(fileName: String) -> URL {
     
          let documentsPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!//.appendingPathComponent("AllFiles")
       //   let documentsPath = URL(string: self.createTodayFolder())
      
      return   documentsPath ;
     }
    
    
    static func getBasePath( basePath:String ) -> String {
        
        let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
        let documentsDirectory: AnyObject = paths[0] as AnyObject
        let dataPath = documentsDirectory.appending("/\(basePath)")
        return dataPath
    }
    
    
    //
    // MARK EMAIL FUNCTIONS WITH DELEGATE
    //
    
    static func sendEmail(viewCon:UIViewController, fileUrl: URL?,operationType:Int) {
           
           if MFMailComposeViewController.canSendMail() {
               
               let mail = MFMailComposeViewController()
               mail.mailComposeDelegate = viewCon as? MFMailComposeViewControllerDelegate
              
               if(operationType == 0){  // action sheet action
                   let attachmentData = NSData(contentsOfFile: fileUrl!.path)
                   let fileName : String = String(fileUrl!.absoluteString.split(separator: "/").last!)
                   let mimetype =  self.MimeTypefrom(filename: fileName)
                   mail.addAttachmentData(attachmentData! as Data, mimeType: mimetype, fileName: fileName)
                   mail.setMessageBody("<p>You're so awesome!</p>", isHTML: true)
               }
               else if(operationType == 1) // user feedback and suggestion
               {
                   mail.setToRecipients(["rashed084050@gmail.com"])
                   mail.title = "User Feedback and Suggestion"
                   mail.setMessageBody("<p>Your Feedback and Suggestions is Our Motivation</p>", isHTML: true)
               }
               else if(operationType == 2) // Tell A Friend to share this app
               {
                   
                   mail.title = "Smart document scanning App Share"
                   mail.setMessageBody("https://apps.apple.com/us/app/smart-document-scanning/id1067569662", isHTML: true)
               }
               
               viewCon.present(mail, animated: true, completion: {
                   
                   print("test ")
                   
               })
           } else {
                
           }
       }

    
       static func MimeTypefrom(filename:String)-> String{
           
           var mimeType : String = ""
           if ( filename == "jpg" ) {
               mimeType =  "image/jpeg"
           } else if (filename == "png" ) {
               mimeType = "image/png";
           } else if (filename == "doc" ) {
               mimeType = "application/msword";
           } else if (filename == "ppt" ) {
               mimeType = "application/vnd.ms-powerpoint";
           } else if (filename == "html" ) {
               mimeType =  "text/html"
           } else if (filename == "pdf" ) {
               mimeType = "application/pdf";
           }
           
           return mimeType
       }
       
       func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
           controller.dismiss(animated: true)
       }
    

       //
      // MARK: SET NAVIGATION BAR PROPERTY
      //
        @objc
       static func setNavigationBarProperty(navbar:UINavigationController!, size: Int ,title:String){
           
           let ssize  = CGFloat(size)
           let attrs = [
               NSAttributedString.Key.foregroundColor:getTextColor(),NSAttributedString.Key.font: UIFont(name:getAppTextFontName(), size:ssize)!
            ]
           navbar.navigationBar.titleTextAttributes = attrs
           navbar.navigationBar.topItem?.title = title
           navbar.navigationBar.backgroundColor = getAppColor()
           navbar.navigationBar.barTintColor = getAppColor()
       }
       
  

      private static  func getHexaStringToColor(hexa: String) -> UIColor {

                   // Convert hex string to an integer
                   let hexint = Int(self.intFromHexString(hexStr: hexa))  //
                   let red = CGFloat((hexint & 0xff0000) >> 16) / 255.0
                   let green = CGFloat((hexint & 0xff00) >> 8) / 255.0
                   let blue = CGFloat((hexint & 0xff) >> 0) / 255.0
       
                   // Create color object, specifying alpha as well
                   let color = UIColor(red: red, green: green, blue: blue, alpha: 1)
                   return color
               }
       
         @objc
         static  func getAppColor() -> UIColor {

                // Convert hex string to an integer
                let hexint = Int(self.intFromHexString(hexStr: APP_HEXA_COlOR_CODE))  //
                let red = CGFloat((hexint & 0xff0000) >> 16) / 255.0
                let green = CGFloat((hexint & 0xff00) >> 8) / 255.0
                let blue = CGFloat((hexint & 0xff) >> 0) / 255.0
    
                // Create color object, specifying alpha as well
                let color = UIColor(red: red, green: green, blue: blue, alpha: 1)
                return color
            }
         @objc
         static  func getTextColor() -> UIColor {

                   // Convert hex string to an integer
                   let hexint = Int(self.intFromHexString(hexStr: APP_TEXT_HEXA_COlOR_CODE))
                   let red = CGFloat((hexint & 0xff0000) >> 16) / 255.0
                   let green = CGFloat((hexint & 0xff00) >> 8) / 255.0
                   let blue = CGFloat((hexint & 0xff) >> 0) / 255.0

                   // Create color object, specifying alpha as well
                   let color = UIColor(red: red, green: green, blue: blue, alpha: 1)
                   return color
               }
         static func intFromHexString(hexStr: String) -> UInt32 {
                var hexInt: UInt32 = 0
                // Create scanner
                let scanner: Scanner = Scanner(string: hexStr)
                // Tell scanner to skip the # character
                scanner.charactersToBeSkipped = CharacterSet(charactersIn: "#")
                // Scan hex value
                scanner.scanHexInt32(&hexInt)
                return hexInt
            }
       private static func getAppTextFontName()-> String{
           
           return "HelveticaNeue-Bold"
          //  return "MarkerFelt-Wide"
         // return  "Georgia-Bold"
       }
         
      static func getAppName()-> String{
             
             return "PDF MASTER"
            //  return "MarkerFelt-Wide"
           // return  "Georgia-Bold"
         }
       @objc  static func SetButtonUIProperty(button: UIButton,title:String,fontSize:Float){
             
             let fontSize = CGFloat(fontSize)
             button.backgroundColor =  getAppColor()//  C80000
             if(title != ""){
                 button.setTitle(title, for: .normal)
             }
             button.setTitleColor(getTextColor(), for: .normal)
             button.titleLabel!.font = UIFont.init(name:getAppTextFontName()  , size:fontSize)
             button.layer.cornerRadius = 10
              
         }
    
    
    
    
      static func addBannerInViewToBottom(viewController: UIViewController) {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.adMobBannerView.rootViewController = viewController
            appDelegate.adMobBannerView.frame = CGRect(x: 0.0,
                                                       y: viewController.view.frame.height - appDelegate.adMobBannerView.frame.height,
                                                       width: viewController.view.frame.width,
                                     height: appDelegate.adMobBannerView.frame.height)
           appDelegate.adMobBannerView.isHidden = false
           viewController.view.addSubview(appDelegate.adMobBannerView)

        }
    
    //
    // MARK: USER DEFAULT PREFERENCE
    //
      
      static func setDocumentDownloadClick(click:Bool){
          
            let defaults = UserDefaults.standard
            defaults.set(click, forKey: "downloadclick")
      }
      @objc
      static func getDocumentDownloadClick()->Bool{
                
             let defaults = UserDefaults.standard
             if (defaults.object(forKey: "downloadclick") != nil) {
                return defaults.bool(forKey:"downloadclick") as Bool
            }
            return false
      }
    @objc
    static func getPdfDocumentPassword()->String{
              
           let defaults = UserDefaults.standard
           if (defaults.object(forKey: "pdfpassword") != nil) {
                return defaults.object(forKey: "pdfpassword") as! String
              }
           return ""
       }
    @objc
         static func setPdfDocumentPassword(password:String){
             
               let defaults = UserDefaults.standard
               defaults.set(password, forKey: "pdfpassword")
         }
    
    
    
    /*
    
 
    
    
      
   
    
    @objc
  static  func clearAllTemporaryFile() {
        let fileManager = FileManager.default
    
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
               let fileNewPath = "\(documentsPath)/\(tempDirectoryName)"
        
        do {
            try fileManager.removeItem(atPath: fileNewPath)
        } catch (let er) {
            print(er)
        }
    }
    
    /*
     @objc
    static func getFileSavingDestinationPath(_ fileName: String) -> String {
        
       // let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let basePath = self.createTodayFolder()
        let filePath = "\(basePath)/\(fileName).pdf"
        return filePath
    }
    */
    
    
   
    
    
    @objc
    static func saveImageAfterEditing(_ fileName: String ,imgFile:UIImage) -> String {

        let basePath = self.getBasePath(basePath:directoryName)
        let filePath = "\(basePath)/\(fileName).png"
        
        let imgdata :Data = imgFile.jpegData(compressionQuality: 1)!
        
        do {
            try imgdata.write(to: URL(fileURLWithPath: filePath) )
            print("file temp destination \(filePath)");
        }
        catch (let e) {
            print("data wirting error is %@",e)
        }
        
        return filePath
    }
     @objc
    static func saveOriginalImage(_ fileName: String ,imgFile:UIImage) -> String {
        
        let basePath = self.getBasePath(basePath:tempDirectoryName)
        let filePath = "\(basePath)/\(fileName).png"
        ///\(tempDirectoryName)
        let imgdata :Data = imgFile.jpegData(compressionQuality: 1)!
        
        do {
            try imgdata.write(to: URL(fileURLWithPath: filePath) )
            print("file temp destination \(filePath)");
        }
        catch (let e) {
            print("data wirting error is %@",e)
        }
        
        return filePath
    }
    
     @objc
    static func getFileSavingDestinationPath(_ fileName: String , fileExtention: String) -> String {
        
       // let basePath = self.getBasePath(basePath: directoryName)
        let basePath = self.createTodayFolder();
        let filePath = "\(basePath)/\(fileName).\(fileExtention)"
        return filePath
    }
    
    static func getOCRDocumentTemporarySavingPath(_ fileName: String) -> String {
          
          let basePath = self.getBasePath(basePath: tempDirectoryName)
          let filePath = "\(basePath)/\(fileName)"
          return filePath
    }
     @objc
    static func getImageFromTemporaryFilePath(_ fileName: String) -> UIImage {
        
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let filePath = "\(documentsPath)/\(tempDirectoryName)/\(fileName).png"
        
        let tempImg :UIImage = UIImage.init(contentsOfFile: filePath)!
        return tempImg
    }
     @objc
    static func saveFileAtTemporaryPath(_ fileName: String) -> String {
        
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let filePath = "\(documentsPath)/\(tempDirectoryName)/\(fileName).png"
        return filePath
    }
    
     @objc
    static func get() -> NSMutableArray    {
        
        var allDirList = NSMutableArray()
        allDirList = self.getAllDirectores()
        
        let documentsUrl = self.getBasePath(basePath: directoryName).url
        
        // var basePath = "";
        // basePath = basePath.appendingFormat("%@/%@", directoryName,allDirList[0] as! String)
        // basePath = self.getBasePath(basePath: basePath)
        
        do {
            // Get the directory contents urls (including subfolders urls)
            let directoryContents = try FileManager.default.contentsOfDirectory( at: documentsUrl, includingPropertiesForKeys: nil, options: [])
            print(directoryContents)
            
            // if you want to filter the directory contents you can do like this:
            let mp3Files = directoryContents.filter{ $0.pathExtension == "pdf" }
            print(" pdf urls:",mp3Files)
            // let mp3FileNames = mp3Files.flatMap({$0.URLByDeletingPathExtension?.lastPathComponent})
            print("pdf list:", mp3Files)
            
            //finalList = mp3Files as! NSMutableArray
            
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        return []
        
    }
    
   
    
     @objc
    static  func getTodayDate( ) -> String {
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.YYYY"
        let  todayDateStr = formatter.string(from: date)
        return todayDateStr.isEmpty ? "" : todayDateStr
    }
     @objc
    static func createTodayFolder() -> String {
        
        var basePath = "";
        basePath = basePath.appendingFormat("%@/%@", directoryName,self.getTodayDate())
        basePath = self.getBasePath(basePath: basePath)
        
        do {
            
            try FileManager.default.createDirectory(atPath:basePath, withIntermediateDirectories: false, attributes: nil)
            print("success")
            
        } catch let error as NSError {
            print(error.localizedDescription);
            
        }
        return basePath;
        
    }
    
    

    
     @objc
    static func  saveImageAsPDFFile(_ imagelist: [String], fileName: String) -> String{

        let filePath :String  = self.getFileSavingDestinationPath(fileName, fileExtention: "pdf");
        
        do {
          //  let pass = PDFPassword.init(self.getPdfDocumentPassword() as String)
            let pdfData = try PDFGenerator.generated(by: imagelist, dpi:.default, password:nil)
            try pdfData.write(to: URL(fileURLWithPath: filePath) )
            print("PDF file destination \(filePath)");
        }
        catch (let e) {
            print("pdf data wirting error is %@",e)
            return e.localizedDescription
        }
        return filePath
        
    }
    
    static func  saveOCRResult( ocrResultStr: String, fileName: String, fileType: String) -> FileInfo{
    
      let filePath :String  = self.getFileSavingDestinationPath(fileName, fileExtention: fileType);
         var aFile : FileInfo
        aFile = FileInfo.init(name: fileName, type: fileType, url: URL.init(fileURLWithPath: filePath))
      do {
          let pdfData =   ocrResultStr.data(using: .utf8)
        try pdfData!.write(to: URL(fileURLWithPath: filePath) )
 
          print("PDF file destination \(filePath)");
      }
      catch (let e) {
          print("pdf data wirting error is %@",e)
        
      }
      return aFile
      
  }

   
   
 
     
    
    
    
    static  func drawOnPDF(path: String) -> [UIImage]
    {
        print(path)
        let urlstr: NSURL = NSURL.init(fileURLWithPath: path)
        
        let pdf: CGPDFDocument = CGPDFDocument(urlstr)!
        var page: CGPDFPage;
        var frame: CGRect = CGRect(x: 0, y: 0, width: 100, height: 200)
        let pageCount: Int = pdf.numberOfPages;
        var array = [String]()
        var imagelist = [UIImage]()
        for i in 1..<pageCount
        {
            
            let mypage: CGPDFPage = pdf.page(at: i + 1)!// CGPDFPage(pdf, i+1)
            frame = mypage.getBoxRect(CGPDFBox.mediaBox)
            UIGraphicsBeginImageContext(CGSize(width: 600, height: 600*(frame.size.height/frame.size.width)))
            let ctx = UIGraphicsGetCurrentContext()!
            //let ctx: CGContext = UIGraphicsGetCurrentContext as! CGContext
            ctx.saveGState()
            ctx.translateBy(x: 0.0, y: frame.size.height)
            ctx.scaleBy(x: 1.0, y: -1.0)
            ctx.setFillColor(gray: 1.0, alpha: 1.0)
            ctx.fill(frame)
            page = pdf.page(at: i + 1)!
            var pdfTransform: CGAffineTransform = page.getDrawingTransform(CGPDFBox.mediaBox, rect: frame, rotate: 0, preserveAspectRatio: true)
            ctx.concatenate(pdfTransform);
           // CGContextSetInterpolationQuality(ctx, kCGInterpolationHigh)
           // CGContextSetRenderingIntent(ctx, kCGRenderingIntentDefault)
            ctx.drawPDFPage(page)
            let thumbnailImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
            ctx.restoreGState()
            
            let documentsPath = self.getOCRDocumentTemporarySavingPath("Page".appendingFormat("%d.png", i+1))
            //NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).last!
           // documentsPath = documentsPath.appendingFormat("/Page%d.png", i+1)
            UIGraphicsEndImageContext()
            var imagedata = NSData()
            imagedata = thumbnailImage.pngData()! as NSData
            
            imagedata.write(toFile: documentsPath, atomically: true)
            array.append(documentsPath)
            imagelist.append(thumbnailImage)
            //.addObject(documentsPath)
            
        }
       // let dirPath = array.objectAtIndex(0) as? String
      //  let image    = UIImage(contentsOfFile: dirPath!)
        UserDefaults.standard.set(array, forKey: "Array")
        print("\(array)")
        return imagelist
    }
    
 
    
  
    
    
    
    
    
    
    
  // MARK: USERDEAFULT PREFERENCE DATA

       @objc
       static func setUserDefaultPreference(autoCropOnOff:String){
           
           let defaults = UserDefaults.standard
           defaults.set(autoCropOnOff, forKey: "autoCropOnOff")
       }
       @objc
       static func getUserDefaultPreference()->Bool{
              
            let defaults = UserDefaults.standard
    
           if (defaults.object(forKey: "autoCropOnOff") != nil) {
               
               let autoOnOff : String =  defaults.object(forKey: "autoCropOnOff") as! String
                      return (autoOnOff as NSString).boolValue
              }
           return true
       }
       
       @objc
       static func setOCRCameraPhotoSaveInLibrary(OnOff:String){
              
              let defaults = UserDefaults.standard
              defaults.set(OnOff, forKey: "ocrPhotoSaveOnOff")
              
              
          }
        @objc
        static func getOCRCameraPhotoSaveInLibrary()->Bool{
                 
              let defaults = UserDefaults.standard
              if (defaults.object(forKey: "ocrPhotoSaveOnOff") != nil) {
                  
                  let autoOnOff : String =  defaults.object(forKey: "ocrPhotoSaveOnOff") as! String
                         return (autoOnOff as NSString).boolValue
                 }
              return true
          }
       
       @objc
       static func setPdfDocumentPassword(password:String){
           
             let defaults = UserDefaults.standard
             defaults.set(password, forKey: "pdfpassword")
       }
       
    
    
    
    */
    
    
}

